#include <console/cmddef.h>
#include "commands/lp_txn_conf.h"

/** Set all outputs */
int cmd_txn_conf(console_ctx_t *ctx, cmd_signature_t *reg) {
    static struct {
        struct arg_lit *add;
        struct arg_lit *remove;
        struct arg_str *outputs;
        struct arg_end *end;
    } args;

    if (reg) {
        args.add = arg_lit0("a", "add", "Add print options to the current list");
        args.remove = arg_lit0("r", "remove", "Remove print options from the current list");
        args.outputs = arg_str0(NULL, NULL, "<options>", EXPENDABLE_STRING(
                "Transaction print options, comma-separated lost of (abbreviated): requests, hex, json, pretty, detailed, verbose. Omit to see the current settings."));
        args.end = arg_end(3);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Configure how CSP transactions are displayed.");
        return 0;
    }

    if (args.outputs->count) {
        // strtok needs mutable buffer. This is a hack, but we know it's safe.
        if (!parse_txn_display_opts((char *) args.outputs->sval[0], args.add->count ? 1 : (args.remove ? -1 : 0))) {
            return CONSOLE_ERR_INVALID_ARG;
        }
        console_println("Txn display options changed.");
    }

    // Show the settings
    if (cmd_lp_txn_conf.txn_show_requests) console_print("requests ");
    if (cmd_lp_txn_conf.txn_show_hex) console_print("hex ");
    if (cmd_lp_txn_conf.txn_show_json) console_print("json ");
    if (cmd_lp_txn_conf.txn_show_pretty) console_print("pretty ");
    if (cmd_lp_txn_conf.txn_annotated) console_print("detailed ");
    if (cmd_lp_txn_conf.txn_verbose) console_print("verbose");
    console_print("\n");

    return CONSOLE_OK;
}
