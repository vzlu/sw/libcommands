#include <console/cmddef.h>
#include <csp/csp.h>

#include <pld/payloads.h>
#include <cJSON.h>
#include <assert.h>

#include "commands/cmd_lp_generic_txn.h"
#include "commands/lp_txn_conf.h"

#if PLD_HAVE_EXPORT
#include <malloc.h>
#endif

#if !PLD_HAVE_ALLOC
// TODO better handling for platforms without alloc
#error "Alloc support in pld required for lp_generic_txn!"
#endif

#if PLD_HAVE_EXPORT
int cmd_lp_generic_txn_print(const struct pld_spec *spec, void* buff, bool req) {
    char* printed = NULL;
    pld_error_t exp_rv = 0;
    cJSON *json = NULL;
    enum pld_export_json_style style = cmd_lp_txn_conf.txn_annotated ? EXPORT_JSON_ANNOTATED : EXPORT_JSON_DATAONLY;

    if (cmd_lp_txn_conf.txn_show_requests) {
        if (buff != NULL) {
            /* Show request as JSON */
            exp_rv = pld_to_json_by_spec(spec, buff, style, &json);
            if (exp_rv != PLD_OK) {
                console_color_printf(COLOR_RED, "Error exporting: %s\n", pld_error_str(exp_rv));
                return CONSOLE_ERROR;
            }

            if (cmd_lp_txn_conf.txn_show_json) {
                printed = cJSON_Print(json);
                if (!printed) {
                    cJSON_Delete(json);
                    return CONSOLE_ERROR;
                }
                if (cmd_lp_txn_conf.txn_verbose) {
                    if(req) {
                        console_print("Request (JSON):\n");
                    } else {
                        console_print("Response (JSON):\n");
                    }
                }
                console_print(printed);
            }

            if (cmd_lp_txn_conf.txn_show_pretty) {
                printed = pld_pretty_print(json, true);
                if (!printed) {
                    return CONSOLE_ERR_NO_MEM;
                }
                if (cmd_lp_txn_conf.txn_verbose) {
                    if(req) {
                        console_print("Request (JSON):\n");
                    } else {
                        console_print("Response (JSON):\n");
                    }
                }
                console_print(printed);
            }
        }
        else {
            if (cmd_lp_txn_conf.txn_verbose) {
                console_println("No request struct given.");
            }
        }
    }

    if (json) {
        cJSON_Delete(json);
    }

    if (printed) {
        console_free(printed);
    }

    return CONSOLE_OK;
}
#endif

int cmd_lp_generic_txn(const struct lp_generic_txn *txn)
{
    return cmd_lp_generic_txn_addr(txn, -1);
}

int cmd_lp_generic_txn_addr(const struct lp_generic_txn *txn, int dst) {
  int return_value = 0;

#define GOTO_FAIL(__x) do { return_value = __x; goto fail; } while(0)
    assert(txn->req_spec);

    pld_error_t pld_rv = 0;

    struct pld_address addr = txn->req_spec->address;
    if(dst >= 0) {
      addr.dst = (uint8_t)dst;
    }

    int timeout = txn->timeout ? txn->timeout : CONSOLE_CSP_DEF_TIMEOUT_MS;

    /* freed variables must be declared and set to their initial value before the first call of GOTO_FAIL! */

    // signed to support csp error codes
    int resp_real_binsz = 0;
    size_t resp_expected_binsz = 0;
    if(txn->resp_spec) {
        if (txn->resp_len == 0) {
            if (txn->resp_spec->growable) {
                resp_expected_binsz = -1;
            } else {
                resp_expected_binsz = (int) txn->resp_spec->bin_size;
            }
        } else {
            resp_expected_binsz = txn->resp_len;
        }
    }
    uint8_t *resp_binbuf = NULL;
    if (txn->resp_spec && txn->resp_spec->bin_size) {
        // Get a buffer from CSP to avoid allocating. All CSP buffers are the same size, we can't go wrong here.
        resp_binbuf = csp_buffer_get(csp_buffer_data_size());
    }
    uint8_t *resp = NULL;

    uint8_t *req_binbuf = NULL;
    size_t req_binsz = 0;
    /* Encode - not using the static version to ensure we allocate the right size for growable structs! */
    pld_rv = txn->req_spec->build(txn->req, console_malloc, &req_binbuf, &req_binsz);
    if (pld_rv != PLD_OK) {
        console_color_printf(COLOR_RED, "Error building request: %s\n", pld_error_str(pld_rv));
        GOTO_FAIL(CONSOLE_ERR_IO);
    }

    if (req_binsz) {
        #if PLD_HAVE_EXPORT
        cmd_lp_generic_txn_print(txn->req_spec, (void *) txn->req, true);
        #endif

        if (cmd_lp_txn_conf.txn_show_requests && cmd_lp_txn_conf.txn_show_hex) {
            console_printf("Sending %d byte%s to [addr %d port %d], timeout %d ms:\n",
                (int)req_binsz, req_binsz>1?"s":"",  addr.dst, addr.dport, timeout);
            console_hexdump(req_binbuf, req_binsz);
        }

        resp_real_binsz = csp_transaction(CSP_PRIO_NORM, addr.dst, addr.dport, timeout,
                                          req_binbuf, (int) req_binsz,
                                          resp_binbuf, resp_expected_binsz);

        console_free(req_binbuf);
        req_binbuf = NULL;
    } else {
        if (cmd_lp_txn_conf.txn_show_requests && cmd_lp_txn_conf.txn_verbose) {
            console_printf("Sending empty request to [addr %d port %d], timeout %d ms\n", addr.dst, addr.dport, timeout);
        }

        resp_real_binsz = csp_transaction(CSP_PRIO_NORM, addr.dst, addr.dport, timeout,
                                          NULL, 0,
                                          resp_binbuf, resp_expected_binsz);
    }

    // csp_transaction returns "1" if no reply was expected and sending went okay, to distinguish it from 0, which indicates error.
    if (resp_real_binsz == 0) {
        console_color_printf(COLOR_RED, "Transaction error!\n");
        GOTO_FAIL(CSP_ERR_TIMEDOUT); // XXX this could be other error too, it's not distinguished by "csp_transaction"
    }

    if (!txn->resp_spec) {
        if (cmd_lp_txn_conf.txn_verbose) {
            console_println("No response expected, not waiting.");
        }
        return CONSOLE_OK;
    }

    if (cmd_lp_txn_conf.txn_show_hex) {
        console_printf("Binary response (%d bytes):\n", resp_real_binsz);
        console_hexdump(resp_binbuf, resp_real_binsz);
    }

    size_t resp_csz = txn->resp_spec->c_size;
    if (txn->resp_spec->growable) {
        resp_csz += resp_real_binsz - txn->resp_spec->bin_size;
    }
    resp = console_calloc(1, resp_csz);

    /* Parse response */
    pld_rv = txn->resp_spec->parse_static(resp_binbuf, resp_real_binsz, resp);
    if (pld_rv != PLD_OK) {
        console_color_printf(COLOR_RED, "Error parsing response: %s\n", pld_error_str(pld_rv));
        GOTO_FAIL(CONSOLE_ERR_IO);
    }

#if PLD_HAVE_EXPORT
    /* Show response as JSON */
    cmd_lp_generic_txn_print(txn->resp_spec, (void *)resp, false);
#endif

    // pass-through
fail:
    if (req_binbuf) {
        console_free(req_binbuf);
    }

    if (resp) {
        if (txn->resp_out && return_value == CONSOLE_OK) {
            // Response received and parsed, pass it to caller.
            // Caller must free the struct when done with it!
            *txn->resp_out = resp;
        } else {
            console_free(resp);
        }
    }

    if (resp_binbuf) {
        csp_buffer_free(resp_binbuf);
    }

    return return_value;
}
