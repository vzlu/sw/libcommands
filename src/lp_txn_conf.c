#include <stddef.h>
#include <string.h>
#include <console/prefix_match.h>
#include "commands/lp_txn_conf.h"

volatile struct cmd_lp_txn_conf cmd_lp_txn_conf;

bool parse_txn_display_opts(char *topts, int operation) {
    const char *options[] = {
        [0] = "requests",
        [1] = "hex",
        [2] = "json",
        [3] = "pretty",
        [4] = "detailed",
        [5] = "verbose",
        NULL
    };

    if (operation == 0) {
        // clear all and then add ...
        cmd_lp_txn_conf.txn_show_requests = false;
        cmd_lp_txn_conf.txn_show_hex = false;
        cmd_lp_txn_conf.txn_show_json = false;
        cmd_lp_txn_conf.txn_show_pretty = false;
        cmd_lp_txn_conf.txn_annotated = false;
        cmd_lp_txn_conf.txn_verbose = false;
    }

    bool choice_value = operation >= 0;

    char *ts = NULL;
    char *piece = strtok_r(topts, ",", &ts);
    while (piece) {
        int n = prefix_match(piece, options, 0);
        switch (n) {
            case 0: // request
                cmd_lp_txn_conf.txn_show_requests = choice_value;
                break;
            case 1: // hex
                cmd_lp_txn_conf.txn_show_hex = choice_value;
                break;
            case 2: // json
                cmd_lp_txn_conf.txn_show_json = choice_value;
                break;
            case 3: // pretty
                cmd_lp_txn_conf.txn_show_pretty = choice_value;
                break;
            case 4: // detail
                cmd_lp_txn_conf.txn_annotated = choice_value;
                break;
            case 5: // verbose
                cmd_lp_txn_conf.txn_verbose = choice_value;
                break;
            case -1:
            default:
                return false;
        }
        piece = strtok_r(NULL, ",", &ts);
    }

    return true;
}
