#!/bin/bash

mkdir -p src
mkdir -p include

rm src/*
rm -rf include/*

lpldgen

# emulate the pld library structure
mkdir -p include/pld
mv include/payloads.h include/pld/payloads.h 
mv include/payloads_internal.h include/pld/payloads_internal.h 
