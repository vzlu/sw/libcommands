/**
 * Transaction commands logging config
 *
 * Created on 2020/08/17.
 */

#ifndef VCOM_TXN_CONF_H
#define VCOM_TXN_CONF_H

#include <stdbool.h>

/** LibPayload transaction commands config */
struct cmd_lp_txn_conf {
    /* CSP transaction commands output formatting */
    bool txn_show_requests;   //!< Show requests (responses are shown always)
    bool txn_show_hex;        //!< Show request/response as hex
    bool txn_show_json;       //!< Show request/response as JSON
    bool txn_show_pretty;     //!< Show request/response in human-readable "pretty" format
    bool txn_annotated;       //!< Use the annotated form of JSON/pretty format
    bool txn_verbose;         //!< Verbose logging (enable additional text lines)
};

extern volatile struct cmd_lp_txn_conf cmd_lp_txn_conf;

/**
 * Parse a list of comma separated options for transaction display
 * and configure VCOM according to these settings.
 *
 * @param opts - options
 * @param operation - 0=replace, -1=subtract, +1=add
 * @return success (i.e. no invalid options given)
 */
bool parse_txn_display_opts(char *opts, int operation);

#endif //VCOM_TXN_CONF_H
