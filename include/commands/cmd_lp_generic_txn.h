/**
 * Perform a CSP transaction using libpayload definitions
 *
 * Created on 2020/05/25.
 */

#ifndef VCOM_CMD_LP_GENERIC_TXN_H
#define VCOM_CMD_LP_GENERIC_TXN_H

#include <pld/payloads_internal.h>

struct lp_generic_txn {
    const struct pld_spec *req_spec;  //!< Request payload specification
    void *req;                        //!< Request payload data. Not needed if the request has no payload or contains only constants.
    const struct pld_spec *resp_spec; //!< Response payload specification, may be NULL if none is expected.
    /**
     * Expected response length (parameter for CSp transaction)
     *
     * 0 = based on payload spec (will become -1 if the response payload is growable)
     * -1 = any length
     * >0 = custom length
     */
    int resp_len;
    int timeout;                      //!< Request timeout (ms), 0 for default
    /**
     * If set, the parsed response message is not freed, but stored here.
     * The user must call console_free() to dispose of the struct afterwards.
     *
     * Nothing is stored if the transaction or parsing fails.
     */
    void **resp_out;
};

#if PLD_HAVE_EXPORT
/**
 * Print payload struct.
 *
 * @param[in] spec - Payload specification
 * @param[in] buff - Buffer containing the parsed payload struct
 * @param[in] req - Set to true when printing requests
 * @return console status code
 */
int cmd_lp_generic_txn_print(const struct pld_spec *spec, void* buff, bool req);
#endif

/**
 * Perform a CSP transaction using libpayload definitions.
 *
 * The request and response are printed, both as binary and parsed JSON.
 *
 * @param[in] txn - transaction info
 * @return console status code
 */
int cmd_lp_generic_txn(const struct lp_generic_txn *txn);

/**
 * Perform a CSP transaction using libpayload definitions, but force different destination address.
 *
 * @param[in] txn - transaction info
 * @param[in] dst - destination node address or negative value to use libpayload default
 * @return console status code
 */
int cmd_lp_generic_txn_addr(const struct lp_generic_txn *txn, int dst);

#endif //VCOM_CMD_LP_GENERIC_TXN_H
